# yaru-openbox

The first Yaru Openbox theme ever.

## Credits

Thanks to Addy for the close button

## How to install

1. `git clone https://gitlab.com/diamondburned/yaru-openbox ~/.themes/yaru-openbox`
2. `obconf` or `lxappearance` and choose theme